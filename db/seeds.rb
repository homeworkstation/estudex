%w(
  español
  matematicas
  historia
  ciencias/ingenieria
  programación
  ingles
  tesis
  otro
).each do |subject|
  Category.find_or_create_by(name: subject)
  print '.'
end

user = User.create_with(password: '12345678').find_or_create_by(
  email: 'student@estudex.com',
  phone: '6825826848',
  first_name: Faker::Name.first_name,
  last_name: Faker::Name.last_name
)

admin_user = AdminUser.create_with(password: '12345678').find_or_create_by(
  email: 'admin@estudex.com'
)

35.times do
  FactoryGirl.create :homework, user: user
  print '.'
end
