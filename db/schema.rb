# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190329020732) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accounts", force: :cascade do |t|
    t.string "number"
    t.string "clabe"
    t.string "bank"
    t.string "name"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_accounts_on_user_id"
  end

  create_table "active_admin_comments", force: :cascade do |t|
    t.string "namespace"
    t.text "body"
    t.string "resource_type"
    t.bigint "resource_id"
    t.string "author_type"
    t.bigint "author_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace"
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"
  end

  create_table "admin_users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_admin_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true
  end

  create_table "approvals", force: :cascade do |t|
    t.bigint "homework_id"
    t.integer "rating"
    t.text "comment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["homework_id"], name: "index_approvals_on_homework_id"
  end

  create_table "bank_transfers", force: :cascade do |t|
    t.bigint "account_id"
    t.bigint "user_id"
    t.float "amount"
    t.string "comment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "completed", default: false
    t.index ["account_id"], name: "index_bank_transfers_on_account_id"
    t.index ["user_id"], name: "index_bank_transfers_on_user_id"
  end

  create_table "books", force: :cascade do |t|
    t.string "autor"
    t.string "name"
    t.text "description"
    t.string "url"
    t.bigint "homework_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["homework_id"], name: "index_books_on_homework_id"
  end

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "credit_movements", force: :cascade do |t|
    t.bigint "homework_id"
    t.bigint "user_id"
    t.float "amount"
    t.string "action"
    t.string "comments"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "bank_transfer_id"
    t.index ["bank_transfer_id"], name: "index_credit_movements_on_bank_transfer_id"
    t.index ["homework_id"], name: "index_credit_movements_on_homework_id"
    t.index ["user_id"], name: "index_credit_movements_on_user_id"
  end

  create_table "documents", force: :cascade do |t|
    t.string "file_file_name"
    t.string "file_content_type"
    t.integer "file_file_size"
    t.datetime "file_updated_at"
    t.text "description"
    t.string "documentable_type"
    t.bigint "documentable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "deliverable", default: false
    t.bigint "user_id"
    t.index ["documentable_type", "documentable_id"], name: "index_documents_on_documentable_type_and_documentable_id"
    t.index ["user_id"], name: "index_documents_on_user_id"
  end

  create_table "filters", force: :cascade do |t|
    t.string "type"
    t.string "data"
    t.bigint "homework_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["homework_id"], name: "index_filters_on_homework_id"
  end

  create_table "homeworks", force: :cascade do |t|
    t.string "name"
    t.string "status", default: "initialized"
    t.text "description"
    t.float "price"
    t.bigint "user_id"
    t.integer "consultor_id"
    t.string "pre_order_id_token"
    t.string "final_order_id_token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "category_id"
    t.datetime "due_date"
    t.string "error_message"
    t.bigint "accepted_request_id"
    t.string "other_category"
    t.index ["accepted_request_id"], name: "index_homeworks_on_accepted_request_id"
    t.index ["category_id"], name: "index_homeworks_on_category_id"
    t.index ["consultor_id"], name: "index_homeworks_on_consultor_id"
    t.index ["user_id"], name: "index_homeworks_on_user_id"
  end

  create_table "messages", force: :cascade do |t|
    t.text "message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "request_id"
    t.bigint "user_id"
    t.index ["request_id"], name: "index_messages_on_request_id"
    t.index ["user_id"], name: "index_messages_on_user_id"
  end

  create_table "requests", force: :cascade do |t|
    t.bigint "homework_id"
    t.bigint "user_id"
    t.boolean "accepted", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["homework_id"], name: "index_requests_on_homework_id"
    t.index ["user_id"], name: "index_requests_on_user_id"
  end

  create_table "user_infos", force: :cascade do |t|
    t.string "type"
    t.string "data"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_user_infos_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.float "stars"
    t.integer "strikes"
    t.string "phone"
    t.date "birth_date"
    t.string "city"
    t.string "state"
    t.string "average"
    t.boolean "consultor", default: true
    t.string "conekta_id"
    t.string "avatar_file_name"
    t.string "avatar_content_type"
    t.integer "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string "username"
    t.boolean "onboarding", default: true
    t.string "first_name"
    t.string "last_name"
    t.float "credit"
    t.boolean "approved", default: false
    t.string "provider"
    t.string "uid"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true
    t.index ["username"], name: "index_users_on_username", unique: true
  end

  create_table "versions", force: :cascade do |t|
    t.string "item_type", null: false
    t.integer "item_id", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.text "object"
    t.datetime "created_at"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"
  end

  add_foreign_key "accounts", "users"
  add_foreign_key "approvals", "homeworks"
  add_foreign_key "bank_transfers", "accounts"
  add_foreign_key "bank_transfers", "users"
  add_foreign_key "books", "homeworks"
  add_foreign_key "credit_movements", "bank_transfers"
  add_foreign_key "credit_movements", "homeworks"
  add_foreign_key "credit_movements", "users"
  add_foreign_key "documents", "users"
  add_foreign_key "filters", "homeworks"
  add_foreign_key "homeworks", "categories"
  add_foreign_key "homeworks", "requests", column: "accepted_request_id"
  add_foreign_key "homeworks", "users"
  add_foreign_key "homeworks", "users", column: "consultor_id"
  add_foreign_key "messages", "requests"
  add_foreign_key "messages", "users"
  add_foreign_key "requests", "homeworks"
  add_foreign_key "requests", "users"
  add_foreign_key "user_infos", "users"
end
