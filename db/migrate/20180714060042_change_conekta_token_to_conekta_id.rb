class ChangeConektaTokenToConektaId < ActiveRecord::Migration[5.1]
  def change
    rename_column(:users, :conekta_token, :conekta_id)
  end
end
