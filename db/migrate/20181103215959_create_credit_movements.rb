class CreateCreditMovements < ActiveRecord::Migration[5.1]
  def change
    create_table :credit_movements do |t|
      t.belongs_to :homework, foreign_key: true
      t.belongs_to :user, foreign_key: true
      t.float :amount
      t.string :action
      t.string :comments

      t.timestamps
    end
  end
end
