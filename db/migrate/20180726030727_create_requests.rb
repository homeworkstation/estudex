class CreateRequests < ActiveRecord::Migration[5.1]
  def change
    create_table :requests do |t|
      t.belongs_to :homework, foreign_key: true
      t.belongs_to :user, foreign_key: true
      t.boolean :accepted, default: false

      t.timestamps
    end
  end
end
