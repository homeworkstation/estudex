class AddConversationToMessages < ActiveRecord::Migration[5.1]
  def change
    add_reference :messages, :request, foreign_key: true
    add_reference :messages, :user, foreign_key: true
    remove_column(:messages, :sender_id, :integer)
    remove_column(:messages, :receiver_id, :integer)
    remove_column(:messages, :homework_id, :integer)
  end
end
