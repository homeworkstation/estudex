class CreateMessages < ActiveRecord::Migration[5.1]
  def change
    create_table :messages do |t|
      t.integer :sender_id
      t.integer :receiver_id
      t.text :message
      t.belongs_to :homework, foreign_key: true

      t.timestamps
    end
  end
end
