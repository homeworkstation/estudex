class AddCompletedToBankTransfer < ActiveRecord::Migration[5.1]
  def change
    add_column :bank_transfers, :completed, :boolean, default: false
  end
end
