class AddOtherCategoryToHomeworks < ActiveRecord::Migration[5.1]
  def change
    add_column :homeworks, :other_category, :string
  end
end
