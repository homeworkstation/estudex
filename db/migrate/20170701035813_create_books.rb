class CreateBooks < ActiveRecord::Migration[5.1]
  def change
    create_table :books do |t|
      t.string :autor
      t.string :name
      t.text :description
      t.string :url
      t.belongs_to :homework, foreign_key: true

      t.timestamps
    end
  end
end
