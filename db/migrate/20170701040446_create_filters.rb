class CreateFilters < ActiveRecord::Migration[5.1]
  def change
    create_table :filters do |t|
      t.string :type
      t.string :data
      t.belongs_to :homework, foreign_key: true

      t.timestamps
    end
  end
end
