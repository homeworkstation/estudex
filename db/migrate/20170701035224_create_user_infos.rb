class CreateUserInfos < ActiveRecord::Migration[5.1]
  def change
    create_table :user_infos do |t|
      t.string :type
      t.string :data
      t.belongs_to :user, foreign_key: true

      t.timestamps
    end
  end
end
