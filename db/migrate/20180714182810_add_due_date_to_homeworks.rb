class AddDueDateToHomeworks < ActiveRecord::Migration[5.1]
  def change
    add_column :homeworks, :due_date, :datetime
  end
end
