class AddCategoryToHomeworks < ActiveRecord::Migration[5.1]
  def change
    add_reference :homeworks, :category, foreign_key: true
  end
end
