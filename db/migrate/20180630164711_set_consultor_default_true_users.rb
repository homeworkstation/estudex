class SetConsultorDefaultTrueUsers < ActiveRecord::Migration[5.1]
  def up
    change_column_default(:users, :consultor, true)
  end

  def down
    change_column_default(:users, :consultor, false)
  end
end
