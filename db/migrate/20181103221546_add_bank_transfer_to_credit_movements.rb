class AddBankTransferToCreditMovements < ActiveRecord::Migration[5.1]
  def change
    add_reference :credit_movements, :bank_transfer, foreign_key: true
  end
end
