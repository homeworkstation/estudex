class CreateBankTransfers < ActiveRecord::Migration[5.1]
  def change
    create_table :bank_transfers do |t|
      t.belongs_to :account, foreign_key: true
      t.belongs_to :user, foreign_key: true
      t.float :amount
      t.string :comment

      t.timestamps
    end
  end
end
