class AddDeliverableToDocuments < ActiveRecord::Migration[5.1]
  def change
    add_column :documents, :deliverable, :boolean, default: false
  end
end
