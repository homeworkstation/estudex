class CreateHomeworks < ActiveRecord::Migration[5.1]
  def change
    create_table :homeworks do |t|
      t.string :name
      t.string :status, default: 'initialized'
      t.text :description
      t.float :price
      t.belongs_to :user, foreign_key: true
      t.integer :consultor_id
      t.string :pre_order_id_token
      t.string :final_order_id_token

      t.timestamps
    end
  end
end
