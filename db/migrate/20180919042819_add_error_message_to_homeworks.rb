class AddErrorMessageToHomeworks < ActiveRecord::Migration[5.1]
  def change
    add_column :homeworks, :error_message, :string
  end
end
