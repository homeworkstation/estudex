class AddAcceptedRequestIdToHomework < ActiveRecord::Migration[5.1]
  def change
    add_column :homeworks, :accepted_request_id, :bigint
    add_index :homeworks, :accepted_request_id
    add_foreign_key :homeworks, :requests, column: :accepted_request_id
  end
end
