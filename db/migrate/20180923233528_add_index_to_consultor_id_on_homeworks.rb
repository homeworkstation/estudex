class AddIndexToConsultorIdOnHomeworks < ActiveRecord::Migration[5.1]
  def change
    add_index :homeworks, :consultor_id
    add_foreign_key :homeworks, :users, column: :consultor_id
  end
end
