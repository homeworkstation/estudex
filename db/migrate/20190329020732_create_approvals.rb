class CreateApprovals < ActiveRecord::Migration[5.1]
  def change
    create_table :approvals do |t|
      t.belongs_to :homework, foreign_key: true
      t.integer :rating
      t.text :comment

      t.timestamps
    end
  end
end
