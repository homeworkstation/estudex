require 'sidekiq/web'

Rails.application.routes.draw do
  devise_for :users, controllers: { omniauth_callbacks: 'omniauth_callbacks' }
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  devise_scope :user do
    unauthenticated :user do
      root 'front#index', as: :front_page
    end

    authenticated :user do
      root 'homeworks#index', as: :authenticated_user_root
    end
  end

  get 'choose', to: 'choose_registration#new', as: :choose_registration
  post 'conekta_oxxo_pay' => 'oxxo_pay#received'

  resources :consultor_onboarding, only: [:new, :update], path_names: { new: 'first_step' } do
    member do
      post :add_accounts
    end
    collection do
      get :second_step
      get :third_step
      get :fourth_step
    end
  end
  resources :onboarding, except: [:index, :edit, :destroy], path_names: { new: 'first_step' } do
    member do
      get :second_step
      get :fourth_step
      get :fifth_step
      get :sixth_step
      patch :set_main_image
    end
  end
  resources :homeworks do
    member do
      post :process_payment
      put :publish
      delete :unpublish
    end
    resources :requests do
      resources :messages, only: :create
      member do
        post :accept
      end
    end
    resources :documents, only: [:index, :new, :create]
    resources :approvals, only: [:new, :create]
  end
  resources :cards, except: [:show, :edit, :update] do
    member do
      patch :make_default
    end
  end
  resources :bank_transfers, only: [:new, :create]
  resources :accounts, except: [:show, :index]

  mount LetterOpenerWeb::Engine, at: '/mails' if Rails.env.staging? || Rails.env.development?
  mount Sidekiq::Web => '/sidekiq'
  mount ActionCable.server => '/cable'

  root to: 'front#index'
end
