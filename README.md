# Estudex

## Prerequesites

To run the server in development mode, you'll need:

* ruby interpreter version according to `.ruby-version`. Using [rvm] or [rbenv]
is advisable
* PostgreSQL database version 9.3. On OS X, using the [Postgres.app] is the
simplest way to get it running.

## Setting up

* `bundle install` to get all required gems
* `bundle exec rake db:setup` to create the database and seed it with foods, exercises and plans
* `bundle exec rake ci:build:commit` to create the database and seed it with foods, exercises and plans

## Running

`bundle exec rails s`

This will run the Rails server on <http://localhost:3000>


[rvm]: http://rvm.io
[rbenv]: http://rbenv.org
[Postgres.app]: http://postgresapp.com/
