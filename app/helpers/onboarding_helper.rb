# frozen_string_literal: true

module OnboardingHelper
  def link_to_cards
    next_url = current_user.cards.any? ? homeworks_path : sixth_step_onboarding_path

    link_to next_url, class: 'col-md-6' do
      image_tag 'blank.png', class: 'image-icon credit-card'
    end
  end
end
