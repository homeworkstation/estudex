# frozen_string_literal: true

module ApplicationHelper
  BACKGROUNDS = %w(
    school
    student
    school
    board
    blue
    tolga
    green
    utensils
  ).freeze

  ALERT_FLASH_TYPES = {
    'notice'  => 'success',
    'success' => 'success',
    'error'   => 'danger',
    'alert'   => 'danger'
  }.freeze

  def random_background
    BACKGROUNDS.sample
  end

  def alert_type(type)
    ALERT_FLASH_TYPES[type]
  end
end
