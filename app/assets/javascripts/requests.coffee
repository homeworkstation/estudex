class Requests
  constructor: () ->
    match = window.location.pathname.match(/^\/homeworks\/(\d+)\/requests\/(\d+)/)
    new RequestChannel(match[2]) if match
    new RequestChannel($('#default_request').attr('data_request')) unless match
    $(".messages-body").scrollTop(100000)

    $('#message_message').keypress (e) ->
      if e.which == 13
        e.preventDefault()
        $('#new_message').submit()

$(() -> 
  new Requests() 
)
