class Homeworks
  constructor: () ->
    $('#homework_category_id').change (e)->
      value = $(@).find(":selected").text()
      $('#homework_other_category_input').hide()
      $('#homework_other_category_input').show() if value == 'otro'

    $('.gallery-documents').royalSlider
      controlNavigation: 'thumbnails'
      imageScaleMode: 'fit'
      thumbs:
        spacing: 10
        arrowsAutoHide: true

$(() -> new Homeworks())
