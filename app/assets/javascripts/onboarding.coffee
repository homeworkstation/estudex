class Onboarding
  constructor: () ->
    @image_send('#upload_image', '#homework_document_ids')

  image_send: (image_select, image_input)->
    $(document).on 'click', image_select, (e)->
      $(image_input).trigger('click')

    $(document).on 'change', image_input, (e)->
      $(e.target).parents('form').submit()

$(() -> new Onboarding())
