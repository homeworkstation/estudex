class window.RequestChannel
  constructor: (request_id) ->
    App.conversation = App.cable.subscriptions.create channel: 'RequestChannel', request_id: request_id,
      connected: ->
        console.log 'connected...'

      disconnected: ->
        console.log 'disconnected...'

      received: (data) ->
        $(".messages-wrapper").replaceWith(data['message'])
        $(".messages-body").scrollTop(100000)
