# frozen_string_literal: true

ActiveAdmin.register User do
  actions :index, :show, :edit, :destroy
  permit_params :email, :password, :password_confirmation

  scope :consultor
  scope :users

  member_action :block, method: :get do
    resource.update locked_at: Time.zone.now
    redirect_to collection_path, notice: 'Usuario bloqueado'
  end

  member_action :approve, method: :get do
    resource.update approved: true
    redirect_to collection_path, notice: 'Consultor Aprobado!'
  end

  member_action :unlock, method: :get do
    resource.update locked_at: nil
    redirect_to collection_path, notice: 'Usuario bloqueado'
  end

  member_action :strike, method: :get do
    resource.update strikes: resource.strikes.to_i + 1
    redirect_to collection_path, notice: 'Strike agregado'
  end

  show do
    attributes_table do
      row :email
      row :current_sign_in_at
      row :sign_in_count
      row :created_at
      row :consultor
      row :strikes
      row :credit
      row :approved
      row :locked_at
    end

    panel 'Documentos de Verificacion' do
      table_for resource.documents do
        column :id
        attachment_column :file
        column :description
      end
    end

    panel 'Movimientos de Credito' do
      table_for resource.credit_movements do
        column :id
        column :homework
        column :bank_transfer
        column :user
        column :amount
        column :action
      end
    end
  end

  index do
    selectable_column
    id_column
    column :email
    column :full_name
    column :current_sign_in_at
    column :sign_in_count
    column :created_at
    column :approved
    column :consultor
    column :strikes
    column :credit
    column :username
    column :locked_at
    actions defaults: true do |block|
      array =  []
      array << link_to('Aprobar', approve_admin_user_path(block), data: { confirm: 'Estas seguro?' }) if block.consultor? && !block.approved
      array << link_to('Bloquear', block_admin_user_path(block), data: { confirm: 'Estas seguro?' }) unless block.locked_at
      array << link_to('Desloquear', unlock_admin_user_path(block), data: { confirm: 'Estas seguro?' }) if block.locked_at
      array << link_to('Dar Strike', strike_admin_user_path(block), data: { confirm: 'Estas seguro?' })
      array.reduce(:+).html_safe if array.any?
    end
  end

  filter :email
  filter :current_sign_in_at
  filter :sign_in_count
  filter :created_at

  form do |f|
    f.inputs '' do
      f.input :email
      f.input :password
      f.input :password_confirmation
    end
    f.actions
  end
end
