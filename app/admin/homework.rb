# frozen_string_literal: true

ActiveAdmin.register Homework do
  member_action :send_to_market, method: :get do
    resource.return_to_market
    redirect_to collection_path, notice: 'Tarea ha cambiado de estado exitosamente'
  end

  member_action :process_payment, method: :get do
    resource.process_payment
    redirect_to collection_path, notice: 'Tarea ha cambiado de estado exitosamente'
  end

  index do
    selectable_column
    id_column
    column :title
    column :status
    column :user
    column :consultor
    column :category
    column :price
    column :error_message
    actions defaults: true do |block|
      array =  []
      array << link_to('Sent to Market', send_to_market_admin_homework_path(block), data: { confirm: 'Estas seguro?' }) unless block.on_market?
      array << link_to('Reintentar Cobro', process_payment_admin_homework_path(block), data: { confirm: 'Estas seguro?' })
      array.reduce(:+).html_safe if array.any?
    end
  end

  show do
    attributes_table do
      row :title
      row :status
      row :user
      row :consultor
      row :category
      row :price
      row :error_message
    end

    panel 'Entregables' do
      table_for resource.documents.deliverables do
        column :id
        attachment_column :file
        column :description
      end
    end
  end
end
