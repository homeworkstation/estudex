ActiveAdmin.register BankTransfer do
  actions :all, except: [:add, :edit]
  permit_params :completed

  member_action :complete, method: :get do
    resource.update completed: true
    redirect_to collection_path, notice: 'Transferencia completada'
  end

  show do
    attributes_table do
      row :clabe
      row :bank
      row :name
      row :user
      row :comment
      row :credit_movement
      row :completed
    end
  end

  index do
    selectable_column
    id_column
    column :account
    column :user
    column :amount
    column :credit_movement
    column :comment
    column :completed
    actions defaults: true do |block|
      array =  []
      array << link_to('Completar', complete_admin_bank_transfer_path(block), data: { confirm: 'Estas seguro?' })
      array.reduce(:+).html_safe if array.any?
    end
  end
end
