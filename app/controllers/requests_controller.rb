# frozen_string_literal: true

class RequestsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_homework
  before_action :set_request, only: %i[show accept show]

  helper_method :homework, :collection, :homework_request

  def index
    @homework_request = collection.first
  end

  # GET /homeworks/1/requests/1
  def show
    render :index
  end

  # POST /homeworks/1/requests
  def create
    if homework.users << current_user
      redirect_to homework, notice: I18n.t('app.homeworks.requested')
    else
      redirect_to homework, error: I18n.t('app.homeworks.already_requested')
    end
  end

  def accept
    if homework.own?(current_user) && homework_request.update(accepted: true)
      redirect_to homework, notice: I18n.t('app.homeworks.accepted')
    else
      redirect_to homework, error: 'No autorizado'
    end
  end

  private

  attr_reader :homework, :homework_request

  def collection
    @collection ||= homework.own?(current_user) ? owner_collection : requester_collection
  end

  def owner_collection
    homework.requests.page(params[:page]).per(10)
  end

  def requester_collection
    homework.requests.where(user_id: current_user.id).page(params[:page]).per(10)
  end

  def set_request
    @homework_request = homework.requests.find(params[:id])
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_homework
    @homework = Homework.find(params[:homework_id])
  end
end
