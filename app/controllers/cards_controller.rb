# frozen_string_literal: true

class CardsController < ApplicationController
  before_action :authenticate_user!
  helper_method :card

  def index
  end

  def new
    @card = Card.new
    render 'onboarding/sixth_step'
  end

  def create
    if current_user.add_card(card_params[:token])
      redirect_to(homeworks_path)
    else
      redirect_back fallback_location: homeworks_path
    end
  end

  def make_default
    current_user.make_default(params[:id])
    redirect_back(fallback_location: homeworks_path)
  end

  def destroy
    current_user.delete_card(params[:id])
    redirect_back(fallback_location: homeworks_path)
  end

  private

  attr_reader :card

  def card_params
    params.require(:payment_method).permit(:token)
  end
end
