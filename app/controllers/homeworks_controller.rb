# frozen_string_literal: true

class HomeworksController < ApplicationController
  before_action :authenticate_user!
  before_action :first_time_onboarding
  before_action :set_homework, only: %i[edit update destroy process_payment publish approve]

  helper_method :homework, :collection, :can_request_homework?

  # GET /homeworks
  def index
  end

  # GET /homeworks/1
  def show
    @homework = Homework.includes(:requests).find(params[:id])
  end

  # GET /homeworks/new
  def new
    @homework = Homework.new
  end

  # GET /homeworks/1/edit
  def edit
  end

  # POST /homeworks
  def create
    @homework = current_user.homeworks.new(homework_params)

    if @homework.save
      redirect_to @homework, notice: I18n.t('app.homeworks.created')
    else
      render :new
    end
  end

  # PATCH/PUT /homeworks/1
  def update
    if homework.update(homework_params)
      redirect_to homework, notice: I18n.t('app.homeworks.updated')
    else
      render :edit
    end
  end

  # DELETE /homeworks/1
  def destroy
    homework.destroy
    redirect_to homeworks_url, notice: I18n.t('app.homeworks.destroyed')
  end

  def process_payment
    homework.process_payment
    redirect_to homework, notice: I18n.t('app.homeworks.will_retry')
  end

  def publish
    if homework.publish!
      redirect_to homework, notice: I18n.t('app.homeworks.publish')
    else
      render :edit
    end
  end

  private

  attr_reader :homework

  def can_request_homework?
    return unless current_user.consultor
    !homework.requested_by?(current_user) && !homework.own?(current_user) && homework.on_market?
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_homework
    @homework = current_user.homeworks.includes(:requests).find(params[:id])
  end

  def collection
    @homeworks ||= params[:mine] ? user_homeworks : homeworks
  end

  def homeworks
    params[:consultor] ? consultor_homeworks : total_homeworks
  end

  def consultor_homeworks
    @consultor_homeworks ||= Homework.where(consultor_id: current_user.id)
                                     .page(params[:page]).per(16)
  end

  def user_homeworks
    @user_homeworks ||= current_user.homeworks.page(params[:page]).per(16)
  end

  def total_homeworks
    @total_homeworks ||= Homework.on_market.page(params[:page]).per(16)
  end

  # TODO: Add a redirect until we release to the Public
  def first_time_onboarding
    return redirect_to(new_onboarding_path) if current_user.onboarding && !current_user.consultor
    return redirect_to(consultor_path) if current_user.onboarding && current_user.consultor
    return redirect_to(wait_path) if !current_user.approved && current_user.consultor
    redirect_to(wait_path) if Rails.env.production?
  end

  def wait_path
    fourth_step_consultor_onboarding_index_path
  end

  def consultor_path
    new_consultor_onboarding_path
  end

  # Only allow a trusted parameter "white list" through.
  def homework_params
    params.require(:homework).permit(
      :name,
      :description,
      :price,
      :due_date,
      :category_id,
      multiple_documents: [],
      documents_attributes: [:file]
    )
  end
end
