# frozen_string_literal: true

class OmniauthCallbacksController < Devise::OmniauthCallbacksController
  attr_reader :user

  def facebook
    @user = User.from_facebook(request.env['omniauth.auth'], request.env['omniauth.params'])

    if user.persisted?
      flash[:notice] = I18n.t("#{translation_scope}.success", kind: 'Facebook')
      sign_in user
      redirect_to homeworks_path
    else
      flash[:notice] = 'Favor de intentarlo mas tarde'
      redirect_to new_user_registration_path
    end
  end

  def failure
    redirect_to root_path
  end
end
