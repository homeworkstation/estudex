# frozen_string_literal: true

class MessagesController < ApplicationController
  before_action :set_homework
  before_action :set_request

  helper_method :homework_request, :message, :homework

  respond_to :js

  # POST /messages
  def create
    @message = homework_request.messages.new(message_params)

    if message.save
      render :create
    else
      render :new
    end
  end

  private

  attr_reader :homework_request, :message, :homework

  def set_request
    @homework_request = homework.requests.find(params[:request_id])
  end

  def set_homework
    @homework = Homework.find(params[:homework_id])
  end

  # Only allow a trusted parameter "white list" through.
  def message_params
    params.require(:message).permit(:message).merge(user_id: current_user.id)
  end
end
