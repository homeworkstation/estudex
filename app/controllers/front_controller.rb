# frozen_string_literal: true

class FrontController < ApplicationController
  layout 'homepage/application'

  def index; end
end
