# frozen_string_literal: true

class ConsultorOnboardingController < ApplicationController
  before_action :authenticate_user!

  helper_method :account

  def new
  end

  def second_step
  end

  def update
    if current_user.update user_document_params
      redirect_to third_step_consultor_onboarding_index_path
    else
      redirect_back fallback_location: homeworks_path
    end
  end

  def third_step
    @account = current_user.accounts.new
  end

  def add_accounts
    @account = current_user.accounts.new(account_params)

    if account.save
      current_user.update onboarding: false
      redirect_to fourth_step_consultor_onboarding_index_path
    else
      render :add_accounts
    end
  end

  def forth_step
  end

  private

  attr_reader :account

  def user_document_params
    params.require(:onboarding).permit(multiple_documents: [])
  end

  # Only allow a trusted parameter "white list" through.
  def account_params
    params.require(:account).permit(:number, :clabe, :bank, :name)
  end
end
