# frozen_string_literal: true

class AccountsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_account, only: %i[show edit update destroy]

  helper_method :account

  # GET /accounts/new
  def new
    @account = current_user.accounts.new
  end

  # GET /accounts/1/edit
  def edit
  end

  # POST /accounts
  def create
    @account = current_user.accounts.new(account_params)

    if @account.save
      redirect_to edit_user_registration_path, notice: 'Cuenta ha sido agregada'
    else
      render :new
    end
  end

  # PATCH/PUT /accounts/1
  def update
    if account.update(account_params)
      redirect_to edit_user_registration_path, notice: 'Cuenta actualizada'
    else
      render :edit
    end
  end

  # DELETE /accounts/1
  def destroy
    account.destroy
    redirect_to edit_user_registration_path, notice: 'Cuenta borrada'
  end

  private

  attr_reader :account

  # Use callbacks to share common setup or constraints between actions.
  def set_account
    @account = current_user.accounts.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def account_params
    params.require(:account).permit(:number, :clabe, :bank, :name)
  end
end
