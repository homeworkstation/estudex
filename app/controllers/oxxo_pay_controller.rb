# frozen_string_literal: true

class OxxoPayController < ApplicationController
  skip_before_action :verify_authenticity_token

  attr_reader :data

  def received
    @data = JSON.parse(request.body.read)
    if data['type'] == 'order.paid' && homework
      homework.accepted_request.notify_accepted
      homework.update consultor: homework.accepted_request.user
      homework.payed!
      HomeworkMailer.oxxo_pay_received(homework.id).deliver_later
    end

    render json: { message: 'success' }
  end

  def homework
    @homework ||= Homework.find_by(final_order_id_token: data['data']['object']['id'])
  end
end
