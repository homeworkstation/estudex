# frozen_string_literal: true

class DocumentsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_homework

  before_action :authenticate_consultor, only: %i[create new]

  helper_method :homework, :document, :collection

  def index
  end

  def new
    @document = homework.documents.new
  end

  def create
    @document = homework.documents.create(document_params)

    if document.valid?
      homework.update status: 'in_review'
      redirect_to homework, notice: 'Tarea subida! El usuario checara el documento '
    else
      redirect_to homework, error: 'documento invalido'
    end
  end

  private

  attr_reader :homework, :document

  def collection
    @collection = homework.documents.page(params[:page]).per(10)
  end

  def set_homework
    @homework = Homework.find(params[:homework_id])
  end

  def authenticate_consultor
    return if homework.consultor.id == current_user.id
    redirect_to root_path, notice: 'NONAUTHORIZED'
  end

  def document_params
    params.require(:document).permit(:file, :description)
          .merge(user: current_user, deliverable: true)
  end
end
