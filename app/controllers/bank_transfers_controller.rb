# frozen_string_literal: true

class BankTransfersController < ApplicationController
  before_action :authenticate_user!
  before_action :set_bank_transfer, only: %i[show edit update destroy]

  helper_method :bank_transfer

  # GET /bank_transfers/new
  def new
    @bank_transfer = current_user.bank_transfers.new
  end

  # POST /bank_transfers
  def create
    @bank_transfer = current_user.bank_transfers.new(bank_transfer_params)

    if bank_transfer.save
      redirect_to edit_user_registration_path, notice: I18n.t('app.homeworks.transfer_started')
    else
      render :new
    end
  end

  private

  attr_reader :bank_transfer

  # Use callbacks to share common setup or constraints between actions.
  def set_bank_transfer
    @bank_transfer = current_user.bank_transfers.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def bank_transfer_params
    params.require(:bank_transfer).permit(:account_id, :amount, :comment)
  end
end
