# frozen_string_literal: true

class OnboardingController < ApplicationController
  before_action :authenticate_user!
  before_action :set_homework, except: %i[index new create]

  helper_method :homework, :card

  def index
    redirect_to new_onboarding_path
  end

  def new
    @homework = current_user.homeworks.new
  end

  def show
    render :second_step
  end

  def create
    @homework = current_user.homeworks.new homework_params

    if homework.save
      redirect_to second_step_onboarding_path(homework)
    else
      render :new
    end
  end

  def second_step
  end

  def fourth_step
  end

  def fifth_step
  end

  def sixth_step
    @card = Card.new
  end

  def set_main_image
    redirect_to fourth_step_onboarding_path(homework) if homework.update(homework_params)
  end

  def update
    if homework.update(homework_params) && homework.publish! && homework.valid?
      current_user.update onboarding: false
      redirect_to after_onboarding_path(homework)
    else
      render :fourth_step
    end
  end

  private

  attr_reader :homework, :card

  def set_homework
    @homework = current_user.homeworks.find(params[:id])
  end

  def after_onboarding_path(homework = nil)
    return fifth_step_onboarding_path(homework) if current_user.cards.empty?
    homeworks_path
  end

  def homework_params
    params.require(:homework).permit(
      :name,
      :description,
      :price,
      :due_date,
      :category_id,
      :other_category,
      documents_attributes: [:file]
    )
  end
end
