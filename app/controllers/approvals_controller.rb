class ApprovalsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_homework

  helper_method :homework, :approval

  # GET /approvals/new
  def new
    @approval = homework.build_approval
  end

  # POST /approvals
  def create
    @approval = homework.build_approval(approval_params)

    if approval.save
      redirect_to homework, notice: 'Tarea Aprobada, muchas gracias'
    else
      render :new
    end
  end

  private

  attr_reader :homework, :approval

    # Use callbacks to share common setup or constraints between actions.
    def set_homework
      @homework = Homework.find(params[:homework_id])
    end

    # Only allow a trusted parameter "white list" through.
    def approval_params
      params.require(:approval).permit(:rating, :comment)
    end
end
