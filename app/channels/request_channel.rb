# frozen_string_literal: true

class RequestChannel < ApplicationCable::Channel
  def subscribed
    stream_from "request_channel_#{params[:request_id]}"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end
end
