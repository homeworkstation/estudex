# frozen_string_literal: true

# A Sidekiq worker for firing off all the bulk emails email

class ActionCableBroadcastWorker
  include Sidekiq::Worker
  # sidekiq_options retry: false
  sidekiq_options queue: :default, retry: 5, backtrace: true

  # Send the broad message to action cable
  def perform(message_id)
    message = Message.find(message_id)
    broadcast "request_channel_#{message.request_id}", render(message.request)
  end

  # Renders the HTML to be appended in the request
  def render(request)
    ApplicationController.renderer.render request
  end

  # Broadcasts the messages to the corresponding channel
  def broadcast(channel, message)
    ActionCable.server.broadcast channel, message: message
  end
end
