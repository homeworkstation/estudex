# frozen_string_literal: true

class HomeworkPayWorker
  include Sidekiq::Worker

  attr_reader :homework

  def perform(homework_id)
    @homework = Homework.find(homework_id)
    ProcessPayment.run!(homework: homework)
  end
end
