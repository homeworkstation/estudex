# frozen_string_literal: true

class HomeworkMailer < ApplicationMailer
  attr_reader :homework, :order

  def oxxo_pay(homework_id)
    @homework = Homework.find(homework_id)
    @order = homework.order
    mail subject: 'Hemos generado tu recibo de Pago para Oxxo! (Estudex)', to: homework.user.email
  end

  def oxxo_pay_received(homework_id)
    @homework = Homework.find(homework_id)
    @order = homework.order
    mail subject: 'Hemos recibido tu Pago en Oxxo! (Estudex)', to: homework.user.email
  end

  def request_received(request_id)
    @request = Request.find(request_id)
    mail subject: 'Alguien quiere hacer tu tarea en Estudex!', to: @request.homework.user.email
  end

  def payment_failed(homework_id)
    @homework = Homework.find(homework_id)
    mail subject: 'Ha habido un Error al procesar tu pago de tu Tarea!', to: homework.user.email
  end

  def request_message(homework_id)
  end

  def request_accepted(request_id)
    @request = Request.find(request_id)
    mail subject: 'Han aceptado tu solicitud de hacer la tarea en Estudex!', to: @request.user.email
  end

  def homework_delivered(homework_id)
    @homework = Homework.find(homework_id)
    mail subject: 'El Consultor a entregado tu tarea a Estudex!', to: @homework.user.email
  end

  def notify_approved(homework_id)
    @homework = Homework.find(homework_id)
    mail subject: 'El Usuario a aprobado tu tarea a Estudex!', to: @homework.consultor.email
  end

  def consultor_approved(user_id)
    @user = User.find(user_id)
    mail subject: 'Haz sido aceptado como consultor en Estudex!', to: @user.email
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.homework_mailer.failed_payment.subject
  #
  def failed_payment(homework_id)
    @homework = Homework.find(homework_id)
    mail to: homework.user.email
  end
end
