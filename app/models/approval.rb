# == Schema Information
#
# Table name: approvals
#
#  id          :integer          not null, primary key
#  homework_id :integer
#  rating      :integer
#  comment     :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_approvals_on_homework_id  (homework_id)
#
# Foreign Keys
#
#  fk_rails_...  (homework_id => homeworks.id)
#

class Approval < ApplicationRecord
  extend Enumerize
  belongs_to :homework

  enumerize :rating, in: %i(1 2 3 4 5), scope: true

  validates :homework, uniqueness: true
  validates :rating, presence: true

  after_create :approve_homework

  def approve_homework
    homework.approve!
  end
end
