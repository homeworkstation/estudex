# frozen_string_literal: true

class Card
  include ActiveModel::Model
  extend ActiveModel::Translation
  extend ActiveModel::Naming

  attr_accessor :name, :number, :cvc, :exp_month, :year
  validates :name, :number, :cvc, :exp_month, :year, presence: true
end
