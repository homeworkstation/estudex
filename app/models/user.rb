# frozen_string_literal: true

#
# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  stars                  :float
#  strikes                :integer
#  phone                  :string
#  birth_date             :date
#  city                   :string
#  state                  :string
#  average                :string
#  consultor              :boolean          default(TRUE)
#  conekta_id             :string
#  avatar_file_name       :string
#  avatar_content_type    :string
#  avatar_file_size       :integer
#  avatar_updated_at      :datetime
#  username               :string
#  onboarding             :boolean          default(TRUE)
#  first_name             :string
#  last_name              :string
#  credit                 :float
#  approved               :boolean          default(FALSE)
#  provider               :string
#  uid                    :string
#  failed_attempts        :integer          default(0), not null
#  unlock_token           :string
#  locked_at              :datetime
#  confirmation_token     :string
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#
# Indexes
#
#  index_users_on_confirmation_token    (confirmation_token) UNIQUE
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#  index_users_on_unlock_token          (unlock_token) UNIQUE
#  index_users_on_username              (username) UNIQUE
#

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  include Concerns::Conektable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :confirmable, :lockable, :omniauthable, omniauth_providers: [:facebook]

  has_attached_file :avatar,
                    styles: { medium: '300x300>', thumb: '100x100>' },
                    default_url: '/images/:style/missing.png'

  validates_attachment_content_type :avatar,
                                    content_type: /\Aimage\/.*\Z/

  has_paper_trail
  has_many :homeworks, dependent: :destroy
  has_many :consultas, class_name: 'Homework', foreign_key: :consultor_id
  has_many :accounts, dependent: :destroy
  has_many :documents, as: :documentable, dependent: :destroy
  has_many :requests, dependent: :destroy
  has_many :approvals, through: :consultas

  scope :consultor, -> { where consultor: true }
  scope :users, -> { where consultor: false }

  accepts_nested_attributes_for :documents

  has_many :credit_movements, dependent: :destroy
  has_many :bank_transfers, dependent: :destroy

  before_create :generate_username
  after_create :generate_avatar, :create_customer
  after_update :sent_approved_email, if: :approved

  validates :first_name, :last_name, :phone, presence: true
  validates :username, :phone, uniqueness: true

  def full_name
    @full_name ||= "#{first_name} #{last_name}"
  end

  def generate_avatar
    update avatar: Faker::Avatar.image
  end

  def multiple_documents=(sent_documents)
    sent_documents.each { |image| documents.create(file: image) }
  end

  def self.from_facebook(omniauth, params)
    conditions = { email: omniauth.info.email, provider: omniauth.provider, uid: omniauth.uid }

    where('email = :email or (provider = :provider and uid = :uid)', conditions)
      .first_or_create do |user|
      user.first_name = omniauth.info.name
      user.email      = omniauth.info.email
      user.password   = Devise.friendly_token[0, 20]
      user.consultor  = params['consultor'] == 'true'
    end
  end

  def password_required?
    super if uid.blank?
  end

  def sent_approved_email
  end

  def rating
    (approvals.average(:rating) || 5).to_f
  end

  protected

  def generate_username
    self.username = loop do
      random_username = Faker::Internet.user_name
      break random_username unless User.find_by(username: random_username)
    end
  end
end
