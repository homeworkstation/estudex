# frozen_string_literal: true

# == Schema Information
#
# Table name: credit_movements
#
#  id               :integer          not null, primary key
#  homework_id      :integer
#  user_id          :integer
#  amount           :float
#  action           :string
#  comments         :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  bank_transfer_id :integer
#
# Indexes
#
#  index_credit_movements_on_bank_transfer_id  (bank_transfer_id)
#  index_credit_movements_on_homework_id       (homework_id)
#  index_credit_movements_on_user_id           (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (bank_transfer_id => bank_transfers.id)
#  fk_rails_...  (homework_id => homeworks.id)
#  fk_rails_...  (user_id => users.id)
#

class CreditMovement < ApplicationRecord
  extend Enumerize
  belongs_to :homework, optional: true
  belongs_to :bank_transfer, optional: true
  belongs_to :user

  after_create :apply_action

  enumerize :action, predicates: true,
                     in: %i[add substract], scope: true

  validates :action, :amount, presence: true

  def apply_action
    send(action)
  end

  def add
    user.update! credit: (user.credit.to_f + amount)
  end

  def substract
    user.update! credit: (user.credit.to_f - amount)
  end
end
