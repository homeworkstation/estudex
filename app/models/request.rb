# frozen_string_literal: true

#
# == Schema Information
#
# Table name: requests
#
#  id          :integer          not null, primary key
#  homework_id :integer
#  user_id     :integer
#  accepted    :boolean          default(FALSE)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_requests_on_homework_id  (homework_id)
#  index_requests_on_user_id      (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (homework_id => homeworks.id)
#  fk_rails_...  (user_id => users.id)
#

class Request < ApplicationRecord
  has_many :messages, dependent: :destroy
  scope :accepted, -> { where accepted: true }

  belongs_to :homework
  belongs_to :user

  validates :homework, uniqueness: { scope: :user }

  after_create :notify_user

  after_update :homework_accepted, if: :accepted_changed?

  def notify_user
    HomeworkMailer.request_received(id).deliver_later
  end

  def notify_accepted
    HomeworkMailer.request_accepted(id).deliver_later
  end

  def homework_accepted
    homework.update accepted_request: self
    homework.request_accepted
    homework.save!
    homework.process_payment
  end
end
