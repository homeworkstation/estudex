# frozen_string_literal: true

# == Schema Information
#
# Table name: books
#
#  id          :integer          not null, primary key
#  autor       :string
#  name        :string
#  description :text
#  url         :string
#  homework_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_books_on_homework_id  (homework_id)
#
# Foreign Keys
#
#  fk_rails_...  (homework_id => homeworks.id)
#

class Book < ApplicationRecord
  belongs_to :homework
end
