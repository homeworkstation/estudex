# frozen_string_literal: true
# == Schema Information
#
# Table name: bank_transfers
#
#  id         :integer          not null, primary key
#  account_id :integer
#  user_id    :integer
#  amount     :float
#  comment    :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  completed  :boolean          default(FALSE)
#
# Indexes
#
#  index_bank_transfers_on_account_id  (account_id)
#  index_bank_transfers_on_user_id     (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (account_id => accounts.id)
#  fk_rails_...  (user_id => users.id)
#

class BankTransfer < ApplicationRecord
  belongs_to :account
  belongs_to :user

  has_one :credit_movement

  delegate :clabe, :bank, :name, to: :account

  after_create :reduce_credit

  validate :validate_credit, on: :create

  validates :amount, presence: true

  def reduce_credit
    user.credit_movements.create!(
      bank_transfer: self,
      action: 'substract',
      amount: amount
    )
  end

  def validate_credit
    return if amount.to_f <= user.credit
    errors.add(:amount, 'No tienes suficiente credito en tu cuenta')
  end
end
