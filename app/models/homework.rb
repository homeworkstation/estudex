# frozen_string_literal: true
# == Schema Information
#
# Table name: homeworks
#
#  id                   :integer          not null, primary key
#  name                 :string
#  status               :string           default("initialized")
#  description          :text
#  price                :float
#  user_id              :integer
#  consultor_id         :integer
#  pre_order_id_token   :string
#  final_order_id_token :string
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  category_id          :integer
#  due_date             :datetime
#  error_message        :string
#  accepted_request_id  :integer
#  other_category       :string
#
# Indexes
#
#  index_homeworks_on_accepted_request_id  (accepted_request_id)
#  index_homeworks_on_category_id          (category_id)
#  index_homeworks_on_consultor_id         (consultor_id)
#  index_homeworks_on_user_id              (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (accepted_request_id => requests.id)
#  fk_rails_...  (category_id => categories.id)
#  fk_rails_...  (consultor_id => users.id)
#  fk_rails_...  (user_id => users.id)
#

class Homework < ApplicationRecord
  has_paper_trail
  include AASM
  default_scope { order(created_at: :desc) }
  belongs_to :category, optional: true
belongs_to :user
  belongs_to :consultor, class_name: 'User', foreign_key: :consultor_id, optional: true
  belongs_to :accepted_request, class_name: 'Request',
                                foreign_key: :accepted_request_id, optional: true

  scope :on_market, -> { ransack(status_in: ['on_market']).result }

  has_many :documents, as: :documentable, dependent: :destroy
  has_many :requests, dependent: :destroy
  has_many :users, through: :requests
  has_many :credit_movements, dependent: :destroy
  has_one :approval, dependent: :destroy

  accepts_nested_attributes_for :documents

  validates :name, :user, presence: true
  validates :due_date, :price, :name, :description, :category,
            presence: true, on: :update, if: :on_market?

  # after_update :process_payment, if: :accepted_request_id_changed?
  after_update :payment_failed, if: -> { status_changed? && rejected? }
  after_update :give_credit, if: -> { status_changed? && finalized? }

  aasm column: :status do
    state :initialized, initial: true
    state :on_market
    state :pending_payment
    state :in_progress
    state :in_review
    state :finalized
    state :rejected
    state :killed

    event :publish do
      transitions from: :initialized, to: :on_market
    end

    event :request_accepted do
      transitions from: :on_market, to: :pending_payment
    end

    event :reject do
      transitions from: %i[pending_payment rejected], to: :rejected
    end

    event :send_to_revision do
      transitions from: [:in_progress], to: :in_review
    end

    event :payed do
      transitions from: %i[pending_payment rejected], to: :in_progress
    end

    event :approve do
      transitions from: %i[in_progress in_review], to: :finalized
    end
  end

  def return_to_market
    update! status: 'on_market', accepted_request_id: nil
    requests.update_all accepted: false
  end

  def process_payment
    HomeworkPayWorker.perform_async(id)
  end

  def primary_image
    @primary_image ||= documents.images.first
  end

  def multiple_documents=(sent_documents)
    sent_documents.each { |image| documents.create(file: image) }
  end

  def total_cost
    (price * 100).round
  end

  def give_credit
    HomeworkMailer.notify_approved(id).deliver_later

    consultor.credit_movements.create!(
      amount: (price.to_f - (price.to_f * 0.25)),
      homework: self,
      action: 'add'
    )
  end

  def payment_failed
    HomeworkMailer.failed_payment(self.id).deliver_later
  end

  def order
    @order ||= Conekta::Order.find(final_order_id_token) if final_order_id_token
  end

  def requested_by?(user)
    requests.find_by(user_id: user.id)
  end

  def own?(user)
    user_id == user.id
  end
end
