# frozen_string_literal: true

# == Schema Information
#
# Table name: messages
#
#  id         :integer          not null, primary key
#  message    :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  request_id :integer
#  user_id    :integer
#
# Indexes
#
#  index_messages_on_request_id  (request_id)
#  index_messages_on_user_id     (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (request_id => requests.id)
#  fk_rails_...  (user_id => users.id)
#

class Message < ApplicationRecord
  belongs_to :request
  belongs_to :user

  has_one :homework, through: :request

  validates :message, presence: true

  after_commit :send_notification_email, :broadcast_message, on: :create

  def send_notification_email
    # TODO: SEND EMAIL
  end

  def broadcast_message
    ActionCableBroadcastWorker.perform_async(id)
  end
end
