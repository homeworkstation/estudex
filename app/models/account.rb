# frozen_string_literal: true

# == Schema Information
#
# Table name: accounts
#
#  id         :integer          not null, primary key
#  number     :string
#  clabe      :string
#  bank       :string
#  name       :string
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_accounts_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#

class Account < ApplicationRecord
  belongs_to :user

  def name
    "#{read_attribute(:name)} - #{bank} ...#{clabe.try(:last, 4)}"
  end

  validates :clabe, :bank, :name, presence: true
end
