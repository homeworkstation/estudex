# frozen_string_literal: true

# == Schema Information
#
# Table name: documents
#
#  id                :integer          not null, primary key
#  file_file_name    :string
#  file_content_type :string
#  file_file_size    :integer
#  file_updated_at   :datetime
#  description       :text
#  documentable_type :string
#  documentable_id   :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  deliverable       :boolean          default(FALSE)
#  user_id           :integer
#
# Indexes
#
#  index_documents_on_documentable_type_and_documentable_id  (documentable_type,documentable_id)
#  index_documents_on_user_id                                (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#

class Document < ApplicationRecord
  ACCEPTED_DOCUMENT_TYPES = %w(
    image/jpg
    image/png
    image/jpeg
    application/pdf
    application/msword
    application/vnd.openxmlformats-officedocument.wordprocessingml.document
    application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
    application/zip
    application/x-zip
  ).freeze

  STYLES = { medium: '300x300>', thumb: '100x100>' }.freeze

  scope :images, -> { where(file_content_type: %w(image/png image/jpg image/jpeg)) }
  scope :non_images, -> { where.not(file_content_type: %w(image/png image/jpg image/jpeg)) }
  scope :deliverables, -> { where(deliverable: true) }
  scope :non_deliverables, -> { where(deliverable: false) }

  has_attached_file :file,
                    styles: lambda { |a| /\Aimage\/.*\Z/.match?(a.content_type) ? STYLES : {} },
                    default_url: '/images/missing.png'

  validates_attachment :file,
                       presence: true,
                       content_type: { content_type: ACCEPTED_DOCUMENT_TYPES }

  belongs_to :user, optional: true
  belongs_to :documentable, polymorphic: true

  after_create :notify_user, if: :deliverable

  def notify_user
    HomeworkMailer.homework_delivered(documentable.id).deliver_later
  end
end
