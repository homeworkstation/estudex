# frozen_string_literal: true

module Concerns
  module Conektable
    def customer
      @customer ||= Conekta::Customer.find(conekta_id) if conekta_id
    end

    def create_customer
      @customer = Conekta::Customer.create(
        name: "#{first_name} #{last_name}",
        email: email,
        phone: phone
      )
      update(conekta_id: @customer['id']) if @customer
    rescue Conekta::ErrorList => _e
      {}
    end

    def cards
      customer ? customer.payment_sources : []
    end

    def add_card(token)
      return unless customer
      customer.create_payment_source(
        type: 'card',
        token_id: token
      )
    end

    def delete_card(token)
      return unless customer
      customer.payment_sources.map(&:last).select do |e|
        e['id'] == token
      end.last.try(:delete)
    end

    def make_default(token)
      return unless customer
      customer.update default_payment_source_id: token
    end
  end
end
