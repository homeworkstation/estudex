# frozen_string_literal: true

class Onboarding
  include ActiveModel::Model
  attr_accessor :files
  validates :files, presence: true
end
