# frozen_string_literal: true

class OxxoPay < ActiveInteraction::Base
  object :homework

  delegate :user, to: :homework

  def conekta_order
    @conekta_order ||= Conekta::Order.create(
      currency: 'MXN',
      customer_info: { customer_id: user.conekta_id },
      pre_authorize: true,
      line_items: [{
        name: "#{homework.name} - #{homework.id} (HOMEWORK) - #{user.id}",
        unit_price: homework.total_cost,
        quantity: 1
      }],
      charges: [{
        payment_method: {
          type: 'oxxo_cash'
        }
      }]
    )
  end

  def execute
    homework.update!(final_order_id_token: conekta_order.id)
    HomeworkMailer.oxxo_pay(homework.id).deliver_later
  rescue Conekta::ErrorList => e
    homework.reject!
    homework.update error_message: e.details.map(&:message).join(', ')
  end
end
