# frozen_string_literal: true

class ProcessPayment < ActiveInteraction::Base
  object :homework

  delegate :user, to: :homework

  def execute
    user.cards.any? ? credit_card_pay : notify_no_credit_card
  end

  def conekta_order
    @conekta_order ||= Conekta::Order.create(
      currency: 'MXN',
      customer_info: { customer_id: user.conekta_id },
      pre_authorize: true,
      line_items: [{
        name: "#{homework.name} - #{homework.id} (HOMEWORK) - #{user.id}",
        unit_price: homework.total_cost,
        quantity: 1
      }],
      charges: [{
        payment_method: {
          type: 'default'
        }
      }]
    ).authorize_capture
  end

  def credit_card_pay
    homework.update(final_order_id_token: conekta_order.id)
    conekta_order.payment_status == 'paid' ? successful : failed
  rescue Conekta::ErrorList => e
    begin
      homework.reject!
    rescue
      nil
    end
    homework.update error_message: e.details.map(&:message).join(', ')
  end

  def successful
    homework.accepted_request.notify_accepted
    homework.update consultor: homework.accepted_request.user
    homework.payed!
  rescue
    nil
  end

  def failed
    homework.reject!
  rescue
    nil
  end

  def notify_no_credit_card
    OxxoPay.run!(homework: homework)
  end
end
