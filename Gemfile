source 'https://rubygems.org'
ruby '2.4.1'

git_source(:github) do |repo_name|
repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
"https://github.com/#{repo_name}.git"
end

gem 'rails', '~> 5.1.1'

# Server
gem 'puma', '~> 3.7'
gem 'config'
# gem 'webpacker'

# Admin
gem 'activeadmin'
gem 'select2-rails'
gem 'activeadmin_addons'
gem 'activeadmin-select2', github: 'mfairburn/activeadmin-select2'
gem 'responsive_active_admin'
gem 'active_skin'

gem 'aasm'

# Payments
gem 'conekta'
gem 'haml'
gem 'haml-rails', '~> 1.0'

gem 'schema_to_scaffold'

gem 'rollbar'
gem 'letter_opener_web'
# gem 'devise-async'
gem 'omniauth-facebook'

# Assets
gem 'turbolinks', '~> 5'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.2'
gem 'sass-rails', '~> 5.0'
gem 'paperclip', '~> 5.0.0'
gem 'formtastic-bootstrap'
gem 'aws-sdk'
gem 'fog'

gem 'bootstrap-sass', '~> 3.4.1'
gem 'bootstrap-datepicker-rails'
gem 'sassc-rails', '>= 2.1.0'

gem 'realslider'

source 'https://rails-assets.org' do
  gem 'rails-assets-datetimepicker'
  gem 'rails-assets-jquery.payment'
  gem 'rails-assets-gabceb--jquery-browser-plugin'
end

gem 'formulaic', '~> 0.3', require: false
# App
gem 'devise'
gem 'devise-async'
gem 'active_interaction'

# DB
gem 'pg'
gem 'hookup'
gem 'enumerize'
gem 'annotate'
gem 'ransack'
gem 'kaminari'
gem 'cancancan', '~> 1.10'
gem 'sidekiq'
gem 'redis-namespace'

gem 'paper_trail'

gem 'paperclip-rack', require: 'paperclip/rack'

# Seed Data
gem 'faker'
gem 'paranoia'
gem 'factory_girl_rails'
gem 'ocr_space'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'rspec-rails', '~> 3.4'

  gem 'pry'
  gem 'simplecov', require: false
  gem 'simplecov-rcov', require: false
  gem 'simplecov-summary'
  gem 'pry-rails'

  gem 'database_cleaner'
  gem 'fivemat'
end

group :development do
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :test do
  gem 'vcr'
  gem 'webmock'
  gem 'rspec-its'
  gem 'rspec-sidekiq'
  gem 'shoulda-matchers'
end

# CI
gem 'rubocop', require: false
gem 'rubycritic', require: false

group :development do
  gem 'capistrano',         require: false
  gem 'capistrano-rvm',     require: false
  gem 'capistrano-npm',     require: false
  gem 'capistrano-rails',   require: false
  gem 'capistrano-bundler', require: false
  gem 'capistrano-shell', require: false
  gem 'capistrano3-puma',   require: false
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'guard-livereload', '~> 2.5', require: false
  gem 'rack-livereload'
end

gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
