# frozen_string_literal: true

FactoryGirl.define do
  factory :bank_transfer do
    account
    user
    amount 1.5
    comment 'Boom'
  end
end
