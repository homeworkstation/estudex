# frozen_string_literal: true

# == Schema Information
#
# Table name: filters
#
#  id          :integer          not null, primary key
#  type        :string
#  data        :string
#  homework_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_filters_on_homework_id  (homework_id)
#
# Foreign Keys
#
#  fk_rails_...  (homework_id => homeworks.id)
#

FactoryGirl.define do
  factory :filter do
    type ''
    data 'MyString'
    homework nil
  end
end
