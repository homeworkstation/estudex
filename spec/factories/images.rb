# frozen_string_literal: true

FactoryGirl.define do
  factory :image do
    file ''
    description 'MyText'
  end
end
