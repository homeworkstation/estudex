# frozen_string_literal: true

# == Schema Information
#
# Table name: homeworks
#
#  id                   :integer          not null, primary key
#  name                 :string
#  status               :string           default("initialized")
#  description          :text
#  price                :float
#  user_id              :integer
#  consultor_id         :integer
#  pre_order_id_token   :string
#  final_order_id_token :string
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  category_id          :integer
#  due_date             :datetime
#  error_message        :string
#  accepted_request_id  :integer
#
# Indexes
#
#  index_homeworks_on_accepted_request_id  (accepted_request_id)
#  index_homeworks_on_category_id          (category_id)
#  index_homeworks_on_consultor_id         (consultor_id)
#  index_homeworks_on_user_id              (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (accepted_request_id => requests.id)
#  fk_rails_...  (category_id => categories.id)
#  fk_rails_...  (consultor_id => users.id)
#  fk_rails_...  (user_id => users.id)
#

FactoryGirl.define do
  factory :homework do
    name { Faker::Book.title }
    association :consultor, factory: :user
    description { Faker::Lorem.paragraph }
    due_date 5.days.from_now
    price 450
    user
  end
end
