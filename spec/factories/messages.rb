# frozen_string_literal: true

# == Schema Information
#
# Table name: messages
#
#  id         :integer          not null, primary key
#  message    :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  request_id :integer
#  user_id    :integer
#
# Indexes
#
#  index_messages_on_request_id  (request_id)
#  index_messages_on_user_id     (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (request_id => requests.id)
#  fk_rails_...  (user_id => users.id)
#

FactoryGirl.define do
  factory :message do
    sender_id 1
    receiver_id 1
    message 'MyText'
    homework nil
  end
end
