# frozen_string_literal: true

FactoryGirl.define do
  factory :credit_movement do
    homework nil
    user nil
    comments 'MyString'
  end
end
