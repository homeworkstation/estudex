# frozen_string_literal: true

# == Schema Information
#
# Table name: user_infos
#
#  id         :integer          not null, primary key
#  type       :string
#  data       :string
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_user_infos_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#

FactoryGirl.define do
  factory :user_info do
    type ''
    data 'MyString'
    user nil
  end
end
