# frozen_string_literal: true

# == Schema Information
#
# Table name: homeworks
#
#  id                   :integer          not null, primary key
#  name                 :string
#  status               :string           default("initialized")
#  description          :text
#  price                :float
#  user_id              :integer
#  consultor_id         :integer
#  pre_order_id_token   :string
#  final_order_id_token :string
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  category_id          :integer
#  due_date             :datetime
#  error_message        :string
#  accepted_request_id  :integer
#
# Indexes
#
#  index_homeworks_on_accepted_request_id  (accepted_request_id)
#  index_homeworks_on_category_id          (category_id)
#  index_homeworks_on_consultor_id         (consultor_id)
#  index_homeworks_on_user_id              (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (accepted_request_id => requests.id)
#  fk_rails_...  (category_id => categories.id)
#  fk_rails_...  (consultor_id => users.id)
#  fk_rails_...  (user_id => users.id)
#

require 'rails_helper'

describe Homework, type: :model do
  it { is_expected.to belong_to :user }
  it { is_expected.to belong_to :category }
  it { is_expected.to belong_to :consultor }
  it { is_expected.to validate_presence_of :user }
end
