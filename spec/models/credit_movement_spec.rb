# frozen_string_literal: true

require 'rails_helper'

describe CreditMovement, type: :model do
  it { is_expected.to belong_to :user }
  it { is_expected.to belong_to :homework }
  it { is_expected.to validate_presence_of :action }
  it { is_expected.to validate_presence_of :amount }
end
