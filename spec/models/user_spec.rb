# frozen_string_literal: true

# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  stars                  :float
#  strikes                :integer
#  phone                  :string
#  birth_date             :date
#  city                   :string
#  state                  :string
#  average                :string
#  consultor              :boolean          default(TRUE)
#  conekta_id             :string
#  avatar_file_name       :string
#  avatar_content_type    :string
#  avatar_file_size       :integer
#  avatar_updated_at      :datetime
#  username               :string
#  onboarding             :boolean          default(TRUE)
#  first_name             :string
#  last_name              :string
#  credit                 :float
#
# Indexes
#
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#  index_users_on_username              (username) UNIQUE
#

require 'rails_helper'

describe User, type: :model do
  it { is_expected.to have_many :homeworks }
  it { is_expected.to have_many :consultas }
  it { is_expected.to validate_uniqueness_of :username }
  it { is_expected.to validate_presence_of :first_name }
end
