# frozen_string_literal: true

require 'rails_helper'

describe BankTransfer, type: :model do
  it { is_expected.to belong_to :account }
  it { is_expected.to belong_to :user }
end
