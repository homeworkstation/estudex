# frozen_string_literal: true

# == Schema Information
#
# Table name: requests
#
#  id          :integer          not null, primary key
#  homework_id :integer
#  user_id     :integer
#  accepted    :boolean          default(FALSE)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_requests_on_homework_id  (homework_id)
#  index_requests_on_user_id      (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (homework_id => homeworks.id)
#  fk_rails_...  (user_id => users.id)
#

require 'rails_helper'

describe Request, type: :model do
  it { is_expected.to belong_to :homework }
  it { is_expected.to belong_to :user }
end
