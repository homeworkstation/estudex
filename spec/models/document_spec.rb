# frozen_string_literal: true

# == Schema Information
#
# Table name: documents
#
#  id                :integer          not null, primary key
#  file_file_name    :string
#  file_content_type :string
#  file_file_size    :integer
#  file_updated_at   :datetime
#  description       :text
#  documentable_type :string
#  documentable_id   :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  deliverable       :boolean          default(FALSE)
#  user_id           :integer
#
# Indexes
#
#  index_documents_on_documentable_type_and_documentable_id  (documentable_type,documentable_id)
#  index_documents_on_user_id                                (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#

require 'rails_helper'

describe Document, type: :model do
  it { is_expected.to belong_to :user }
  it { is_expected.to belong_to :documentable }
end
