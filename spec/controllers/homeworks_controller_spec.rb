# frozen_string_literal: true

require 'rails_helper'

describe HomeworksController, type: :controller do
  render_views

  let(:valid_attributes) do
    {
      name: 'MyString',
      description: 'MyText',
      price: 1.5
    }
  end

  let(:invalid_attributes) { {} }

  let(:user)          { create :user, onboarding: false }
  let(:valid_session) { sign_in user }

  describe 'GET #index' do
    it 'returns a success response' do
      get :index, params: {}, session: valid_session
      expect(response).to be_success
    end
  end

  describe 'GET #show' do
    let(:other_user) { create :user }
    let(:homework) { create :homework, user: other_user }

    it 'returns a success response' do
      get :show, params: { id: homework.to_param }, session: valid_session
      expect(response).to be_success
    end
  end

  describe 'GET #new' do
    it 'returns a success response' do
      get :new, params: {}, session: valid_session
      expect(response).to be_success
    end
  end

  describe 'GET #edit' do
    let(:homework) { create :homework, user: user }

    it 'returns a success response' do
      get :edit, params: { id: homework.to_param }, session: valid_session
      expect(response).to be_success
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      it 'creates a new Homework' do
        expect do
          post :create, params: { homework: valid_attributes }, session: valid_session
        end.to change(Homework, :count).by(1)
      end

      it 'redirects to the created homework' do
        post :create, params: { homework: valid_attributes }, session: valid_session
        expect(response).to redirect_to(Homework.last)
      end
    end
  end

  describe 'PUT #update' do
    let!(:homework) { create :homework, user: user }

    context 'with valid params' do
      let(:data) do
        {
          id: homework.to_param,
          homework: {
            name: 'pancholeon'
          }
        }
      end

      it 'redirects to the homework' do
        put :update, params: data, session: valid_session
        expect(homework.reload.name).to eq 'pancholeon'
      end
    end
  end

  describe 'DELETE #destroy' do
    let!(:homework) { create :homework, user: user }

    it 'destroys the requested homework' do
      expect do
        delete :destroy, params: { id: homework.to_param }, session: valid_session
      end.to change(Homework, :count).by(-1)
    end

    it 'redirects to the homeworks list' do
      delete :destroy, params: { id: homework.to_param }, session: valid_session
      expect(response).to redirect_to(homeworks_url)
    end
  end
end
