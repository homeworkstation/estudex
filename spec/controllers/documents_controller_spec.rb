# frozen_string_literal: true

require 'rails_helper'

describe DocumentsController, type: :controller do
  render_views

  let(:user) { create :user }
  let(:homework) { create :homework }
  let(:valid_session) { sign_in homework.consultor }
  let(:invalid_session) { sign_in user }

  describe 'GET #new' do
    context 'authenticated consultor' do
      it 'returns http success' do
        get :new, params: { homework_id: homework.id }, session: valid_session
        expect(response).to have_http_status(:success)
      end

      it 'returns http success' do
        get :new, params: { homework_id: homework.id }, session: invalid_session
        expect(response).to_not have_http_status(:success)
      end
    end
  end

  describe 'GET #create' do
    let(:file) { fixture_file_upload('spec/fixtures/image.png') }

    let(:attributes) do
      {
        document: {
          file:  file,
          description: 'BOOM'
        }
      }
    end

    xit 'returns http success' do
      expect do
        get :create, params: attributes.merge(homework_id: homework.id), session: valid_session
      end.to change(Document, :count).by(1)
    end
  end
end
