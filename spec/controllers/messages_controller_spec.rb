# frozen_string_literal: true

require 'rails_helper'

describe MessagesController, type: :controller do
  render_views

  let(:valid_attributes) do
    {
      message: { message: 'Hola' },
      homework_id: homework_request.homework.id,
      request_id: homework_request.id
    }
  end

  let(:invalid_attributes) do
    {
      message: { message: nil },
      homework_id: homework_request.homework.id,
      request_id: homework_request.id
    }
  end

  let(:homework_request) { create :request }
  let(:user) { create :user }
  let(:valid_session) { sign_in user }

  describe 'POST #create' do
    context 'with valid params' do
      it 'creates a new Message' do
        expect do
          post :create, params: valid_attributes, session: valid_session, format: :js
        end.to change(Message, :count).by(1)
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'new' template)" do
        expect do
          post :create, params: invalid_attributes, session: valid_session, format: :js
        end.to change(Message, :count).by(0)
      end
    end
  end
end
