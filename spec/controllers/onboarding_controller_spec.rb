# frozen_string_literal: true

require 'rails_helper'

describe OnboardingController, type: :controller do
  render_views

  let(:user) { create :user }
  let(:valid_session) { sign_in user }
  let(:homework) { create :homework, user: user }

  before do
    request.env['HTTP_REFERER'] = 'http://example.com'
  end

  describe 'GET #index' do
    it 'returns http success' do
      get :new, params: {}, session: valid_session
      expect(response).to have_http_status(:success)
    end
  end

  describe 'GET #show' do
    it 'returns http success' do
      get :show, params: { id: homework.to_param }, session: valid_session
      expect(response).to have_http_status(:success)
    end
  end

  describe 'GET steps' do
    %i(second fourth fifth sixth).each do |step|
      it 'returns http success' do
        get "#{step}_step", params: { id: homework.to_param }, session: valid_session
        expect(response).to have_http_status(:success)
      end
    end
  end
end
