# frozen_string_literal: true

require 'rails_helper'

describe BankTransfersController, type: :controller do
  render_views

  let(:user) { create :user, credit: 57 }
  let!(:account) { create :account, user: user }
  let(:valid_session) { sign_in account.user }

  let(:valid_attributes) do
    {
      account_id: account.id,
      amount: 12
    }
  end

  let(:invalid_attributes) do
    {
      account_id: account.id,
      amount: 91_238_123
    }
  end

  describe 'GET #new' do
    it 'returns a success response' do
      get :new, params: {}, session: valid_session
      expect(response).to be_success
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      it 'creates a new BankTransfer' do
        expect do
          post :create, params: { bank_transfer: valid_attributes }, session: valid_session
        end.to change(BankTransfer, :count).by(1)
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'new' template)" do
        expect do
          post :create, params: { bank_transfer: invalid_attributes }, session: valid_session
        end.to change(BankTransfer, :count).by(0)
      end
    end
  end
end
