# frozen_string_literal: true

require 'rails_helper'

describe AccountsController, type: :controller do
  render_views

  let(:valid_attributes) do
    {
      number: '123',
      clabe: '123',
      bank: 'Santander',
      name: 'JUan pancho'
    }
  end

  let(:invalid_attributes) do
    {
      number: nil,
      clabe: nil,
      bank: nil,
      name: nil
    }
  end

  let(:user) { create :user }
  let(:account) { create :account, user: user }
  let(:valid_session) { sign_in user }

  describe 'GET #new' do
    it 'returns a success response' do
      get :new, params: {}, session: valid_session
      expect(response).to be_success
    end
  end

  describe 'GET #edit' do
    it 'returns a success response' do
      get :edit, params: { id: account.to_param }, session: valid_session
      expect(response).to be_success
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      it 'creates a new Account' do
        expect do
          post :create, params: { account: valid_attributes }, session: valid_session
        end.to change(Account, :count).by(1)
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'new' template)" do
        expect do
          post :create, params: { account: invalid_attributes }, session: valid_session
        end.to change(Account, :count).by(0)
      end
    end
  end

  describe 'PUT #update' do
    context 'with valid params' do
      let(:new_attributes) do
        { name: 'Poncho' }
      end

      let(:data) { { id: account.to_param, account: new_attributes } }

      it 'updates the requested account' do
        put :update, params: data, session: valid_session
        expect(account.reload.read_attribute(:name)).to eq 'Poncho'
      end
    end
  end

  describe 'DELETE #destroy' do
    let!(:account) { create :account, user: user }

    it 'destroys the requested account' do
      expect do
        delete :destroy, params: { id: account.to_param }, session: valid_session
      end.to change(Account, :count).by(-1)
    end
  end
end
