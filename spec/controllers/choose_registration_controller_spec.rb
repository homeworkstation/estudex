# frozen_string_literal: true

require 'rails_helper'

describe ChooseRegistrationController, type: :controller do
  render_views

  describe 'GET #new' do
    it 'returns http success' do
      get :new
      expect(response).to have_http_status(:success)
    end
  end
end
