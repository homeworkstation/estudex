# frozen_string_literal: true

# Preview all emails at http://localhost:3000/rails/mailers/homework
class HomeworkPreview < ActionMailer::Preview
  # Preview this email at http://localhost:3000/rails/mailers/homework/failed_payment
  def failed_payment
    HomeworkMailer.failed_payment
  end

  # Preview this email at http://localhost:3000/rails/mailers/homework/no_credit_cards_added
  def no_credit_cards_added
    HomeworkMailer.no_credit_cards_added
  end
end
