# frozen_string_literal: true

require 'rails_helper'

RSpec.describe HomeworkMailer, type: :mailer do
  describe 'failed_payment' do
    let(:mail) { HomeworkMailer.failed_payment }

    xit 'renders the headers' do
      expect(mail.subject).to eq('Failed payment')
      expect(mail.to).to eq(['to@example.org'])
      expect(mail.from).to eq(['from@example.com'])
    end
  end

  describe 'no_credit_cards_added' do
    let(:mail) { HomeworkMailer.no_credit_cards_added }

    xit 'renders the headers' do
      expect(mail.subject).to eq('No credit cards added')
      expect(mail.to).to eq(['to@example.org'])
      expect(mail.from).to eq(['from@example.com'])
    end
  end
end
