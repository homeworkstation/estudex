# frozen_string_literal: true

require 'rails_helper'

describe BankTransfersController, type: :routing do
  describe 'routing' do
    it 'routes to #new' do
      expect(get: '/bank_transfers/new').to route_to('bank_transfers#new')
    end

    it 'routes to #create' do
      expect(post: '/bank_transfers').to route_to('bank_transfers#create')
    end
  end
end
