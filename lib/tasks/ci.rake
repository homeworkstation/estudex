# frozen_string_literal: true

unless Rails.env.production? || Rails.env.staging?
  require 'rubocop/rake_task'
  require 'rubycritic/rake_task'
  require 'rspec/core/rake_task'

  namespace :ci do
    namespace :build do
      desc 'Clean logs'
      task :clean do
        File.exist?('log') || FileUtils.mkdir('log')
        FileUtils.rm_rf(Dir.glob('log/test.log'))
        FileUtils.rm_rf(Dir.glob('log/coverage*'))
        FileUtils.rm_rf(Dir.glob('log/rubocop.log'))
      end

      RuboCop::RakeTask.new(:rubocop) do |t|
        t.options = %w(-foffenses -fprogress -olog/rubocop.log)
        t.fail_on_error = true
      end

      RubyCritic::RakeTask.new do |task|
        task.options = '--mode-ci --no-browser -s 90 --format json'
        task.verbose = true
      end

      # Do not show this task in the task list
      RSpec::Core::RakeTask.new(:spec_internal) do |t|
        t.rspec_opts = '--profile'
      end

      desc 'Run rspec with code coverage analysis and profile option'
      task :simplecov do
        ENV['COVERAGE'] = 'true'
        Rake::Task['ci:build:spec_internal'].invoke
      end

      desc 'Run all tests'
      task commit: ['ci:build:clean', 'ci:build:rubycritic', 'ci:build:simplecov']
    end
  end
end
